class TaskModel {
  String title;
  String? description;
  String label;
  String boardName;
  String? timeSpent;
  bool? isFinished;
  String? dateFinished;


  TaskModel({
    required this.title,
    this.description,
    required this.label,
    required this.boardName,
    this.timeSpent,
    this.isFinished,
    this.dateFinished,
  });

  factory TaskModel.fromJson(Map<String, dynamic> json) {
    return TaskModel(
      title: json['title'],
      description: json['description'],
      label: json['label'],
      boardName: json['boardName'],
      timeSpent: json['timeSpent'] ?? "0",
      isFinished: json['isFinished'] ?? false,
      dateFinished: json['dateFinished'] ?? ""
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'title': title,
      'description': description,
      'label': label,
      'boardName': boardName,
      'timeSpent': timeSpent,
      'isFinished': isFinished,
      'dateFinished': dateFinished,
    };
  }
}
