import 'package:kanban_board_challenge/data/models/task_model.dart';

class UserModel {
  final String email;
  final List<TaskModel> tasks;

  UserModel({
    required this.email,
    required this.tasks,
  });

  factory UserModel.fromJson(Map<String, dynamic> json) {
    List<TaskModel> tmpTasks = [];
    if (json['tasks'] != null) {
      for (int i = 0; i < json['tasks'].length; i++) {
        tmpTasks.add(TaskModel.fromJson(json['tasks'][i]));
      }
    }

    List<TaskModel> tmpCompletedTasks = [];
    if (json['completedTasks'] != null) {
      for (int i = 0; i < json['completedTasks'].length; i++) {
        tmpCompletedTasks.add(TaskModel.fromJson(json['completedTasks'][i]));
      }
    }

    return UserModel(
      email: json['email'],
      tasks: tmpTasks,
    );
  }
}
