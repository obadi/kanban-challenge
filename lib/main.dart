import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:kanban_board_challenge/firebase_options.dart';
import 'package:kanban_board_challenge/screens/add_task_screen/helpers/add_task_provider.dart';
import 'package:kanban_board_challenge/screens/auth_screen/helpers/auth_provider.dart';
import 'package:kanban_board_challenge/screens/home_screen/helpers/home_provider.dart';
import 'package:kanban_board_challenge/screens/splash_screen/helpers/splash_provider.dart';
import 'package:kanban_board_challenge/screens/task_screen/helpers/task_provider.dart';
import 'package:kanban_board_challenge/utils/app_constant.dart';
import 'package:kanban_board_challenge/utils/route_generator.dart';
import 'package:provider/provider.dart';
import 'package:sizer/sizer.dart';

String mainPath = "";
GlobalKey<NavigatorState>? navigatorKey = GlobalKey<NavigatorState>();

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(
    options: DefaultFirebaseOptions.currentPlatform,
  );

  runApp(
    MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (_) => SplashProvider()),
        ChangeNotifierProvider(create: (_) => HomeProvider()),
        ChangeNotifierProvider(create: (_) => AuthProvider()),
        ChangeNotifierProvider(create: (_) => TaskProvider()),
        ChangeNotifierProvider(create: (_) => AddTaskProvider()),
      ],
      child: const MyApp(),
    ),
  );
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return Sizer(builder: (context, orientation, deviceType) {
      return MaterialApp(
        debugShowCheckedModeBanner: false,
        initialRoute: RouteNames.splashScreen,
        onGenerateRoute: RouteGenerator.generateRoute,
        title: 'Kanban Board',
        theme: ThemeData(
          platform: TargetPlatform.iOS,
          primaryColor: AppConstant.mainColor,
        ),
      );
    });
  }
}
