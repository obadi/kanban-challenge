import 'dart:convert';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'package:kanban_board_challenge/data/models/task_model.dart';
import 'package:kanban_board_challenge/data/models/user_model.dart';
import 'package:kanban_board_challenge/utils/app_constant.dart';

class AddTaskProvider with ChangeNotifier {
  User? loggedUser;

  String title = "";
  String description = "";
  int chosenLabel = -1;
  String boardName = "";

  setBoardName(String boardName) {
    this.boardName = boardName;
  }

  setChosenLabel(int index, BuildContext context) {
    chosenLabel = index;
    Navigator.pop(context);
    notifyListeners();
  }

  setTitle(String title) {
    this.title = title;
  }

  setDescription(String description) {
    this.description = description;
  }

  getChosenLabel() {
    if (chosenLabel == 0) {
      return "Bug";
    } else if (chosenLabel == 1) {
      return "Development";
    } else if (chosenLabel == 2) {
      return "Design";
    }
  }

  getLoggedUser() {
    loggedUser = FirebaseAuth.instance.currentUser;
  }

  addTask() async {
    TaskModel newTask = TaskModel(
      title: title,
      label: getChosenLabel(),
      description: description,
      boardName: boardName,
      timeSpent: "0",
      isFinished: boardName == AppConstant.done ? true : false,
      dateFinished: ""
    );
    List<TaskModel> oldTasks = [];

    DatabaseReference ref = FirebaseDatabase.instance.ref();

    final snapshot = await ref.child('users/${loggedUser!.uid}').get();
    if (snapshot.exists) {
      String stringJson = jsonEncode(snapshot.value);
      UserModel userModel = UserModel.fromJson(jsonDecode(stringJson));
      oldTasks = userModel.tasks;
    }
    oldTasks.add(newTask);
    List<Map<String, dynamic>> newTasks = [];
    for (int i = 0; i < oldTasks.length; i++) {
      newTasks.add(oldTasks[i].toJson());
    }

    await ref.update({
      "users/${loggedUser!.uid}/tasks": newTasks,
    });
  }
}
