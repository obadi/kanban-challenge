import 'package:flutter/material.dart';
import 'package:kanban_board_challenge/screens/add_task_screen/helpers/add_task_provider.dart';
import 'package:kanban_board_challenge/utils/app_constant.dart';
import 'package:kanban_board_challenge/utils/widgets/loading.dart';
import 'package:kanban_board_challenge/utils/widgets/main_button.dart';
import 'package:provider/provider.dart';
import 'package:sizer/sizer.dart';

class AddTaskScreen extends StatefulWidget {
  final String boardName;

  const AddTaskScreen({Key? key, required this.boardName}) : super(key: key);

  @override
  State<AddTaskScreen> createState() => _AddTaskScreenState();
}

class _AddTaskScreenState extends State<AddTaskScreen> {
  AddTaskProvider? _addTaskProvider;

  final _formKey = GlobalKey<FormState>();

  @override
  void didChangeDependencies() {
    _addTaskProvider = Provider.of<AddTaskProvider>(context, listen: true);
    _addTaskProvider!.getLoggedUser();
    _addTaskProvider!.setBoardName(widget.boardName);
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppConstant.offWhite,
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.transparent,
        toolbarHeight: 75.sp,
        leading: IconButton(
          onPressed: () {
            Navigator.pop(context);
          },
          icon: const Icon(
            Icons.close,
            color: AppConstant.mainColor,
          ),
        ),
      ),
      body: Container(
        height: MediaQuery.of(context).size.height,
        margin: EdgeInsets.symmetric(horizontal: 15.sp),
        child: SingleChildScrollView(
          child: Form(
            key: _formKey,
            child: Column(
              children: [
                Container(
                  decoration: BoxDecoration(
                    color: AppConstant.grey,
                    borderRadius: BorderRadius.circular(12.5),
                  ),
                  padding: EdgeInsets.symmetric(horizontal: 5.sp),
                  child: TextFormField(
                    decoration: const InputDecoration(
                      hintText: AppConstant.titleHint,
                      border: InputBorder.none,
                    ),
                    onChanged: (value) {
                      _addTaskProvider!.setTitle(value);
                    },
                    validator: (value) {
                      if (value == null || value.isEmpty) {
                        return AppConstant.emptyTaskTitleError;
                      }
                      return null;
                    },
                  ),
                ),
                SizedBox(
                  height: 20.sp,
                ),
                Container(
                  decoration: BoxDecoration(
                    color: AppConstant.grey,
                    borderRadius: BorderRadius.circular(12.5),
                  ),
                  padding: EdgeInsets.symmetric(horizontal: 5.sp),
                  child: TextFormField(
                    maxLines: 5,
                    onChanged: (value) {
                      _addTaskProvider!.setDescription(value);
                    },
                    decoration: const InputDecoration(
                      hintText: AppConstant.descriptionHint,
                      border: InputBorder.none,
                    ),
                  ),
                ),
                SizedBox(
                  height: 20.sp,
                ),
                InkWell(
                  onTap: chooseLabel,
                  child: Container(
                    decoration: BoxDecoration(
                      color: AppConstant.grey,
                      borderRadius: BorderRadius.circular(12.5),
                    ),
                    padding: EdgeInsets.all(10.sp),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Row(
                          children: [
                            const Icon(
                              Icons.label_important_rounded,
                              color: AppConstant.mainColor,
                            ),
                            SizedBox(
                              width: 5.sp,
                            ),
                            const Text(AppConstant.label)
                          ],
                        ),
                        Row(
                          children: [
                            _addTaskProvider!.chosenLabel == -1
                                ? Container()
                                : Container(
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(50),
                                      color: AppConstant.labelsColors[
                                              _addTaskProvider!
                                                  .getChosenLabel()]!
                                          .withOpacity(0.2),
                                    ),
                                    padding: EdgeInsets.symmetric(
                                        horizontal: 5.sp, vertical: 2.sp),
                                    child: Text(
                                      _addTaskProvider!.getChosenLabel(),
                                      style: Theme.of(context)
                                          .textTheme
                                          .subtitle1!
                                          .copyWith(
                                              color: AppConstant.labelsColors[
                                                  _addTaskProvider!
                                                      .getChosenLabel()]),
                                    ),
                                  ),
                            SizedBox(
                              width: 5.sp,
                            ),
                            const Icon(
                              Icons.arrow_forward_ios,
                              size: 12.5,
                              color: AppConstant.mainColor,
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
                SizedBox(
                  height: 50.sp,
                ),
                MainButton(
                  onPressed: _addTaskProvider!.chosenLabel != -1
                      ? () async {
                          if (_formKey.currentState!.validate()) {
                            showDialog(
                                context: context,
                                builder: (_) {
                                  return const Loading();
                                });
                            await _addTaskProvider!.addTask();
                            Navigator.pop(context);
                            Navigator.pop(context);
                          }
                        }
                      : null,
                  text: AppConstant.addTaskButton,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  chooseLabel() {
    showModalBottomSheet(
        context: context,
        builder: (_) {
          return Column(
            children: [
              SizedBox(
                height: 5.sp,
              ),
              Center(
                child: Text(
                  AppConstant.chooseLabelTitle,
                  style: Theme.of(context).textTheme.titleLarge,
                ),
              ),
              SizedBox(
                height: 15.sp,
              ),
              InkWell(
                onTap: () {
                  _addTaskProvider!.setChosenLabel(0, context);
                },
                child: Container(
                  decoration: BoxDecoration(
                    color: AppConstant.grey,
                    borderRadius: BorderRadius.circular(12.5),
                  ),
                  padding: EdgeInsets.all(10.sp),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Row(
                        children: [
                          const Icon(
                            Icons.label_important_rounded,
                            color: AppConstant.mainColor,
                          ),
                          SizedBox(
                            width: 5.sp,
                          ),
                          Container(
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(50),
                              color: Colors.red.withOpacity(0.2),
                            ),
                            padding: EdgeInsets.symmetric(
                                horizontal: 5.sp, vertical: 2.sp),
                            child: Text(
                              "Bug",
                              style: Theme.of(context)
                                  .textTheme
                                  .subtitle1!
                                  .copyWith(
                                    color: Colors.red,
                                  ),
                            ),
                          ),
                        ],
                      ),
                      Row(
                        children: const [
                          Icon(
                            Icons.arrow_forward_ios,
                            size: 12.5,
                            color: AppConstant.mainColor,
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
              SizedBox(
                height: 5.sp,
              ),
              InkWell(
                onTap: () {
                  _addTaskProvider!.setChosenLabel(1, context);
                },
                child: Container(
                  decoration: BoxDecoration(
                    color: AppConstant.grey,
                    borderRadius: BorderRadius.circular(12.5),
                  ),
                  padding: EdgeInsets.all(10.sp),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Row(
                        children: [
                          const Icon(
                            Icons.label_important_rounded,
                            color: AppConstant.mainColor,
                          ),
                          SizedBox(
                            width: 5.sp,
                          ),
                          Container(
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(50),
                              color: Colors.green.withOpacity(0.2),
                            ),
                            padding: EdgeInsets.symmetric(
                                horizontal: 5.sp, vertical: 2.sp),
                            child: Text(
                              "Development",
                              style: Theme.of(context)
                                  .textTheme
                                  .subtitle1!
                                  .copyWith(
                                    color: Colors.green,
                                  ),
                            ),
                          ),
                        ],
                      ),
                      Row(
                        children: const [
                          Icon(
                            Icons.arrow_forward_ios,
                            size: 12.5,
                            color: AppConstant.mainColor,
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
              SizedBox(
                height: 5.sp,
              ),
              InkWell(
                onTap: () {
                  _addTaskProvider!.setChosenLabel(2, context);
                },
                child: Container(
                  decoration: BoxDecoration(
                    color: AppConstant.grey,
                    borderRadius: BorderRadius.circular(12.5),
                  ),
                  padding: EdgeInsets.all(10.sp),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Row(
                        children: [
                          const Icon(
                            Icons.label_important_rounded,
                            color: AppConstant.mainColor,
                          ),
                          SizedBox(
                            width: 5.sp,
                          ),
                          Container(
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(50),
                              color: Colors.yellow.withOpacity(0.2),
                            ),
                            padding: EdgeInsets.symmetric(
                                horizontal: 5.sp, vertical: 2.sp),
                            child: Text(
                              "Design",
                              style: Theme.of(context)
                                  .textTheme
                                  .subtitle1!
                                  .copyWith(
                                    color: Colors.yellow,
                                  ),
                            ),
                          ),
                        ],
                      ),
                      Row(
                        children: const [
                          Icon(
                            Icons.arrow_forward_ios,
                            size: 12.5,
                            color: AppConstant.mainColor,
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
            ],
          );
        });
  }
}
