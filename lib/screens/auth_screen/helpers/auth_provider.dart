import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'package:kanban_board_challenge/main.dart';
import 'package:kanban_board_challenge/utils/route_generator.dart';
import 'package:kanban_board_challenge/utils/widgets/loading.dart';

class AuthProvider with ChangeNotifier {
  signInLogic(BuildContext context) async {
    showLoading(context);
    await FirebaseAuth.instance
        .signInWithProvider(GoogleAuthProvider())
        .then((value) async {
      DatabaseReference ref =
          FirebaseDatabase.instance.ref("users/${value.user!.uid}");

      await ref.set({
        "email": value.user!.email,
      });
    });
    Navigator.pop(context);
    Navigator.pushReplacementNamed(context, RouteNames.homeScreen);
  }

  showLoading(BuildContext context) {
    showDialog(
      context: context,
      builder: (_) {
        return const Loading();
      },
    );
  }
}
