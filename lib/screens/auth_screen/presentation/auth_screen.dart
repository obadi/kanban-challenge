import 'package:flutter/material.dart';
import 'package:kanban_board_challenge/screens/auth_screen/helpers/auth_provider.dart';
import 'package:kanban_board_challenge/utils/app_constant.dart';
import 'package:kanban_board_challenge/utils/widgets/main_button.dart';
import 'package:provider/provider.dart';
import 'package:sizer/sizer.dart';

class AuthScreen extends StatefulWidget {
  const AuthScreen({Key? key}) : super(key: key);

  @override
  State<AuthScreen> createState() => _AuthScreenState();
}

class _AuthScreenState extends State<AuthScreen> {
  AuthProvider? _taskProvider;

  @override
  void didChangeDependencies() {
    _taskProvider = Provider.of<AuthProvider>(context, listen: true);
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        backgroundColor: AppConstant.offWhite,
        leading: Container(),
      ),
      backgroundColor: AppConstant.offWhite,
      body: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        margin: EdgeInsets.symmetric(horizontal: 15.sp),
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Text(
                AppConstant.authTitle,
                style: Theme.of(context)
                    .textTheme
                    .titleLarge!
                    .copyWith(fontSize: 24.sp),
                textAlign: TextAlign.center,
              ),
              SizedBox(
                height: 350.sp,
              ),
              MainButton(
                onPressed: () {
                  _taskProvider!.signInLogic(context);
                },
                text: AppConstant.logInButton,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
