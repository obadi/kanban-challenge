import 'dart:convert';
import 'dart:io';
import 'package:drag_and_drop_lists/drag_and_drop_lists.dart';
import 'package:excel/excel.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'package:kanban_board_challenge/data/models/task_model.dart';
import 'package:kanban_board_challenge/data/models/user_model.dart';
import 'package:kanban_board_challenge/screens/home_screen/widgets/footer_card.dart';
import 'package:kanban_board_challenge/screens/home_screen/widgets/home_card.dart';
import 'package:kanban_board_challenge/utils/app_constant.dart';
import 'package:kanban_board_challenge/utils/route_generator.dart';
import 'package:open_file/open_file.dart';
import 'package:path_provider/path_provider.dart';

class HomeProvider with ChangeNotifier {
  User? loggedUser;
  UserModel? userModel;
  List<TaskModel> userTasks = [];
  final List<DragAndDropList> contents = [
    DragAndDropList(
      header: const Text(AppConstant.todo),
      children: [],
      footer: const FooterCard(boardName: AppConstant.todo),
      canDrag: false,
    ),
    DragAndDropList(
      header: const Text(AppConstant.inProgress),
      children: [],
      footer: const FooterCard(boardName: AppConstant.inProgress),
      canDrag: false,
    ),
    DragAndDropList(
      header: const Text(AppConstant.done),
      children: [],
      footer: const FooterCard(boardName: AppConstant.done),
      canDrag: false,
    ),
  ];

  onItemReorder(
      int oldItemIndex, int oldListIndex, int newItemIndex, int newListIndex) {
    return;
  }

  onListReorder(int oldListIndex, int newListIndex) {
    return;
  }

  getLoggedUser() {
    loggedUser = FirebaseAuth.instance.currentUser;
  }

  getUserData() async {
    final ref = FirebaseDatabase.instance.ref();
    final snapshot = await ref.child('users/${loggedUser!.uid}').get();
    if (snapshot.exists) {
      userTasks.clear();
      String stringJson = jsonEncode(snapshot.value);
      userModel = UserModel.fromJson(jsonDecode(stringJson));
      userTasks = userModel!.tasks;
      fillTasks();
    } else {
      print('No data available.');
    }
  }

  fillTasks() {
    contents.elementAt(0).children.clear();
    contents.elementAt(1).children.clear();
    contents.elementAt(2).children.clear();
    for (int i = 0; i < userTasks.length; i++) {
      if (userTasks[i].boardName == AppConstant.todo) {
        contents.elementAt(0).children.add(
              DragAndDropItem(
                child: HomeCard(
                  task: userTasks[i],
                ),
                canDrag: false,
              ),
            );
      } else if (userTasks[i].boardName == AppConstant.inProgress) {
        contents.elementAt(1).children.add(
              DragAndDropItem(
                child: HomeCard(
                  task: userTasks[i],
                ),
                canDrag: false,
              ),
            );
      } else if (userTasks[i].boardName == AppConstant.done) {
        contents.elementAt(2).children.add(
              DragAndDropItem(
                child: HomeCard(
                  task: userTasks[i],
                ),
                canDrag: false,
              ),
            );
      }
    }

    notifyListeners();
  }

  exportData() async {
    // final stream = Stream.fromIterable(userTasks);
    // final csvRowStream = stream.transform(ListToCsvConverter());
    //
    // String csv = const ListToCsvConverter().convert(userTasks!);

    var excel = Excel.createExcel();

    Sheet sheetObject = excel['Sheet1'];

    List<String> dataList = [
      "boardName",
      "title",
      "description",
      "label",
      "timeSpent",
      "isFinished",
      "dateFinished"
    ];

    sheetObject.insertRowIterables(dataList, 0);
    for (int i = 0; i < userTasks.length; i++) {
      List<String> tmpRow = [
        userTasks[i].boardName,
        userTasks[i].title,
        userTasks[i].description ?? "",
        userTasks[i].label,
        userTasks[i].timeSpent ?? "",
        userTasks[i].isFinished.toString(),
        userTasks[i].dateFinished ?? "",
      ];
      sheetObject.insertRowIterables(tmpRow, i+1);
    }

    var fileBytes = excel.save();
    var directory = await getApplicationDocumentsDirectory();

    final File file = File('${directory.path}/my_file.xlsx');
    await file.writeAsBytes(fileBytes!);
    OpenFile.open(file.path);
  }

  logout(BuildContext context) async {
    await FirebaseAuth.instance.signOut();
    Navigator.pushReplacementNamed(context, RouteNames.authScreen);
  }
}
