import 'package:drag_and_drop_lists/drag_and_drop_lists.dart';
import 'package:flutter/material.dart';
import 'package:kanban_board_challenge/screens/home_screen/helpers/home_provider.dart';
import 'package:kanban_board_challenge/utils/app_constant.dart';
import 'package:kanban_board_challenge/utils/route_generator.dart';
import 'package:provider/provider.dart';
import 'package:sizer/sizer.dart';

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key});

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  HomeProvider? _homeProvider;

  @override
  void didChangeDependencies() {
    _homeProvider = Provider.of<HomeProvider>(context, listen: true);
    _homeProvider!.getLoggedUser();
    _homeProvider!.getUserData();
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppConstant.offWhite,
      appBar: AppBar(
        elevation: 0,
        leadingWidth: 0,
        backgroundColor: Colors.transparent,
        title: InkWell(
          onTap: () {
            _homeProvider!.logout(context);
          },
          child: const Align(
            alignment: Alignment.centerLeft,
            child: Text(
              AppConstant.homePageTitle,
              style: TextStyle(color: AppConstant.mainColor),
            ),
          ),
        ),
        actions: [
          IconButton(
            onPressed: _homeProvider!.exportData,
            icon: Icon(Icons.import_export, color: AppConstant.mainColor,),
          )
        ],
      ),
      body: RefreshIndicator(
        onRefresh: (){
          _homeProvider!.getUserData();
          return Future.value();
        },
        child: Container(
          height: MediaQuery.of(context).size.height,
          margin: EdgeInsets.symmetric(horizontal: 15.sp),
          child: ListView(
            children: [
              SizedBox(
                height: 15.sp,
              ),
              SizedBox(
                child: Text(
                  "${AppConstant.welcome} ${_homeProvider!.loggedUser!.providerData[0].email!.split("@")[0]}",
                  style: Theme.of(context)
                      .textTheme
                      .titleLarge!
                      .copyWith(fontSize: 22.sp),
                ),
              ),
              SizedBox(
                height: 15.sp,
              ),
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.8,
                child: DragAndDropLists(
                  children: _homeProvider!.contents,
                  onItemReorder: _homeProvider!.onItemReorder,
                  onListReorder: _homeProvider!.onListReorder,
                  listPadding: EdgeInsets.symmetric(vertical: 15.sp),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
