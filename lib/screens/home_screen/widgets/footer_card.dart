import 'package:flutter/material.dart';
import 'package:kanban_board_challenge/utils/app_constant.dart';
import 'package:kanban_board_challenge/utils/route_generator.dart';
import 'package:sizer/sizer.dart';

class FooterCard extends StatelessWidget {
  final String boardName;

  const FooterCard({Key? key, required this.boardName}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: 50.sp,
      decoration: BoxDecoration(
          color: AppConstant.grey.withOpacity(0.1),
          borderRadius: BorderRadius.circular(15)),
      margin: EdgeInsets.only(top: 10.sp),
      child: InkWell(
        onTap: () {
          Navigator.pushNamed(context, RouteNames.addTaskScreen,
              arguments: {'boardName': boardName});
        },
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 10.sp, vertical: 5.sp),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: const [
              Icon(
                Icons.add,
                color: AppConstant.grey,
              ),
              Text(AppConstant.addCard)
            ],
          ),
        ),
      ),
    );
  }
}
