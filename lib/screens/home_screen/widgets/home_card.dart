import 'package:flutter/material.dart';
import 'package:kanban_board_challenge/data/models/task_model.dart';
import 'package:kanban_board_challenge/screens/home_screen/helpers/home_provider.dart';
import 'package:kanban_board_challenge/utils/app_constant.dart';
import 'package:kanban_board_challenge/utils/route_generator.dart';
import 'package:provider/provider.dart';
import 'package:sizer/sizer.dart';

class HomeCard extends StatefulWidget {
  final TaskModel task;

  const HomeCard({Key? key, required this.task}) : super(key: key);

  @override
  State<HomeCard> createState() => _HomeCardState();
}

class _HomeCardState extends State<HomeCard> {
  HomeProvider? _homeProvider;

  @override
  void didChangeDependencies() {
    _homeProvider = Provider.of<HomeProvider>(context, listen: false);
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: 70.sp,
      margin: EdgeInsets.only(top: 10.sp),
      child: InkWell(
        onTap: () {
          Navigator.pushNamed(context, RouteNames.taskScreen, arguments: {
            'task': TaskModel(
              title: widget.task.title,
              label: widget.task.label,
              description: widget.task.description,
              boardName: widget.task.boardName,
              timeSpent: widget.task.timeSpent,
              isFinished: widget.task.isFinished,
              dateFinished: widget.task.dateFinished,
            )
          });
        },
        child: Card(
          elevation: 5,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(15.0),
          ),
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: 10.sp, vertical: 5.sp),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  widget.task.title,
                  style: Theme.of(context).textTheme.titleLarge,
                ),
                Container(
                  decoration: BoxDecoration(
                    color: AppConstant.labelsColors[widget.task.label]!
                        .withOpacity(0.2),
                    borderRadius: BorderRadius.circular(50),
                  ),
                  padding:
                      EdgeInsets.symmetric(horizontal: 5.sp, vertical: 2.sp),
                  child: Text(
                    widget.task.label,
                    style: Theme.of(context).textTheme.subtitle1!.copyWith(
                        color: AppConstant.labelsColors[widget.task.label]),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
