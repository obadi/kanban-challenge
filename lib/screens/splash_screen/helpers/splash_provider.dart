import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:kanban_board_challenge/utils/route_generator.dart';

class SplashProvider with ChangeNotifier {
  checkIfLoggedIn(BuildContext context) {
    FirebaseAuth.instance.authStateChanges().listen((User? user) {
      if (user == null) {
        Navigator.pushReplacementNamed(context, RouteNames.authScreen);
      } else {
        Navigator.pushReplacementNamed(context, RouteNames.homeScreen);
      }
    });
  }
}