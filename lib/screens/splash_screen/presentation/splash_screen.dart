import 'package:flutter/material.dart';
import 'package:kanban_board_challenge/screens/splash_screen/helpers/splash_provider.dart';
import 'package:kanban_board_challenge/utils/app_constant.dart';
import 'package:kanban_board_challenge/utils/widgets/loading.dart';
import 'package:provider/provider.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  SplashProvider? _splashProvider;

  @override
  void didChangeDependencies() {
    _splashProvider = Provider.of<SplashProvider>(context, listen: true);
    Future.delayed(const Duration(seconds: 1), () {
      _splashProvider!.checkIfLoggedIn(context);
    });
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return const Scaffold(
      backgroundColor: AppConstant.offWhite,
      body: Center(
        child: Loading(),
      ),
    );
  }
}
