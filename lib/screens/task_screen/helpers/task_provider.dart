import 'dart:async';
import 'dart:convert';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'package:kanban_board_challenge/data/models/task_model.dart';
import 'package:kanban_board_challenge/data/models/user_model.dart';
import 'package:kanban_board_challenge/utils/app_constant.dart';

class TaskProvider with ChangeNotifier {
  User? loggedUser;
  int? chosenLabel;
  String chosenLabelString = "";
  Timer? timer;
  int sec = 0;
  int min = 0;
  int hour = 0;
  String timeSpent = "";
  bool timerStatus = false;

  getLoggedUser() {
    loggedUser = FirebaseAuth.instance.currentUser;
  }

  getTimeSpentOnTask(TaskModel task) {
    timeSpent = task.timeSpent!;
    notifyListeners();
  }

  setChosenLabel(int index, BuildContext context) {
    chosenLabel = index;
    getChosenLabel();
  }

  setTimeSpent(String timeSpent){
    this.timeSpent = timeSpent;
  }

  setChosenLabelString(String chosenLabelString){
    this.chosenLabelString = chosenLabelString;
  }

  getChosenLabel() {
    if (chosenLabel == 0) {
      chosenLabelString = "Bug";
    } else if (chosenLabel == 1) {
      chosenLabelString = "Development";
    } else if (chosenLabel == 2) {
      chosenLabelString = "Design";
    }
    notifyListeners();
  }

  startTimer() {
    timerStatus = true;
    notifyListeners();
    timer = Timer.periodic(Duration(seconds: 1), (timer) {
      if (sec < 59) {
        sec++;
      } else if (sec == 59) {
        sec = 0;
        if (min == 59) {
          hour++;
          min = 0;
        } else {
          min++;
        }
      }

      timeSpent = "$hour:$min:$sec";
      notifyListeners();
    });
  }

  stopTimer() {
    timerStatus = false;
    timer!.cancel();
    notifyListeners();
  }

  editTask(TaskModel task, String newTitle, String newDescription,
      String newBoardName) async {
    DatabaseReference ref = FirebaseDatabase.instance.ref();
    final snapshot = await ref.child('users/${loggedUser!.uid}').get();
    if (snapshot.exists) {
      String stringJson = jsonEncode(snapshot.value);
      UserModel userModel = UserModel.fromJson(jsonDecode(stringJson));
      List<TaskModel> tasks = userModel.tasks;
      for (int i = 0; i < tasks.length; i++) {
        if (tasks[i].title == task.title) {
          final snapshot =
              await ref.child('users/${loggedUser!.uid}/tasks/$i').get();
          String stringJson = jsonEncode(snapshot.value);
          TaskModel taskModel = TaskModel.fromJson(jsonDecode(stringJson));
          taskModel.title = newTitle;
          taskModel.description = newDescription;
          taskModel.label = chosenLabelString;
          taskModel.boardName = newBoardName;
          if(sec != 0 || min != 0 || hour != 0) taskModel.timeSpent = timeSpent;
          if(newBoardName == AppConstant.done) {
            taskModel.dateFinished = DateTime.now().toString();
            taskModel.isFinished = true;
          } else {
            taskModel.dateFinished = "";
            taskModel.isFinished = false;
          }
          await ref.update({
            "users/${loggedUser!.uid}/tasks/$i": taskModel.toJson(),
          });
        }
      }
    }
  }
}
