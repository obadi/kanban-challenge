import 'package:flutter/material.dart';
import 'package:kanban_board_challenge/data/models/task_model.dart';
import 'package:kanban_board_challenge/screens/task_screen/helpers/task_provider.dart';
import 'package:kanban_board_challenge/utils/app_constant.dart';
import 'package:kanban_board_challenge/utils/widgets/loading.dart';
import 'package:kanban_board_challenge/utils/widgets/main_button.dart';
import 'package:provider/provider.dart';
import 'package:sizer/sizer.dart';

class TaskScreen extends StatefulWidget {
  final TaskModel task;

  const TaskScreen({Key? key, required this.task}) : super(key: key);

  @override
  State<TaskScreen> createState() => _TaskScreenState();
}

class _TaskScreenState extends State<TaskScreen> {
  final TextEditingController titleCont = TextEditingController();
  final FocusNode titleNode = FocusNode();
  final TextEditingController descriptionCont = TextEditingController();
  final FocusNode descriptionNode = FocusNode();

  String dropdownvalue = '';

  var items = [
    AppConstant.todo,
    AppConstant.inProgress,
    AppConstant.done,
  ];

  TaskProvider? _taskProvider;

  int labelIndex = -1;

  @override
  void initState() {
    if (widget.task.label == AppConstant.todo)
      labelIndex = 0;
    else if (widget.task.label == AppConstant.inProgress)
      labelIndex = 1;
    else if (widget.task.label == AppConstant.done) labelIndex = 2;
    Provider.of<TaskProvider>(context, listen: false)
        .setChosenLabelString(widget.task.label);
    Provider.of<TaskProvider>(context, listen: false)
        .setTimeSpent(widget.task.timeSpent ?? "0:0:0");
    dropdownvalue = widget.task.boardName;
    super.initState();
  }

  @override
  void didChangeDependencies() {
    _taskProvider = Provider.of<TaskProvider>(context, listen: true);
    _taskProvider!.getLoggedUser();
    descriptionCont.text = widget.task.description ?? "";
    titleCont.text = widget.task.title;
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<TaskProvider>(
      builder: (context, task, snapshot) {
        return Scaffold(
          backgroundColor: AppConstant.offWhite,
          appBar: AppBar(
            elevation: 0,
            backgroundColor: Colors.transparent,
            toolbarHeight: 75.sp,
            leading: IconButton(
              onPressed: () {
                Navigator.pop(context);
              },
              icon: const Icon(
                Icons.close,
                color: AppConstant.mainColor,
              ),
            ),
          ),
          body: Container(
            height: MediaQuery.of(context).size.height,
            margin: EdgeInsets.symmetric(horizontal: 15.sp),
            child: SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  widget.task.isFinished!
                      ? Text("Date finished ${widget.task.dateFinished}")
                      : Container(),
                  EditableText(
                    style: Theme.of(context).textTheme.titleLarge!.copyWith(
                          fontSize: 22.sp,
                          overflow: TextOverflow.ellipsis,
                        ),
                    controller: titleCont,
                    focusNode: titleNode,
                    cursorColor: AppConstant.mainColor,
                    backgroundCursorColor: Colors.cyan,
                    maxLines: 2,
                  ),
                  Container(
                    decoration: BoxDecoration(
                        color: AppConstant.grey,
                        borderRadius: BorderRadius.circular(12.5)),
                    padding: EdgeInsets.symmetric(horizontal: 5.sp),
                    child: DropdownButton(
                      value: dropdownvalue,
                      underline: Container(),
                      icon: const Icon(Icons.keyboard_arrow_down),
                      items: items.map((String items) {
                        return DropdownMenuItem(
                          value: items,
                          child: Text(items),
                        );
                      }).toList(),
                      onChanged: (String? newValue) {
                        setState(() {
                          dropdownvalue = newValue!;
                        });
                      },
                    ),
                  ),
                  SizedBox(
                    height: 25.sp,
                  ),
                  Container(
                    decoration: BoxDecoration(
                        color: AppConstant.grey,
                        borderRadius: BorderRadius.circular(12.5)),
                    padding:
                        EdgeInsets.symmetric(horizontal: 5.sp, vertical: 5.sp),
                    child: EditableText(
                      style: Theme.of(context)
                          .textTheme
                          .subtitle1!
                          .copyWith(overflow: TextOverflow.ellipsis),
                      controller: descriptionCont,
                      focusNode: descriptionNode,
                      cursorColor: AppConstant.mainColor,
                      backgroundCursorColor: Colors.cyan,
                      maxLines: 5,
                    ),
                  ),
                  SizedBox(
                    height: 45.sp,
                  ),
                  InkWell(
                    onTap: chooseLabel,
                    child: Container(
                      decoration: BoxDecoration(
                        color: AppConstant.grey,
                        borderRadius: BorderRadius.circular(12.5),
                      ),
                      padding: EdgeInsets.all(10.sp),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Row(
                            children: [
                              const Icon(
                                Icons.label_important_rounded,
                                color: AppConstant.mainColor,
                              ),
                              SizedBox(
                                width: 5.sp,
                              ),
                              const Text(AppConstant.label)
                            ],
                          ),
                          Row(
                            children: [
                              Container(
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(50),
                                  color: AppConstant.labelsColors[
                                          _taskProvider!.chosenLabelString]!
                                      .withOpacity(0.2),
                                ),
                                padding: EdgeInsets.symmetric(
                                    horizontal: 5.sp, vertical: 2.sp),
                                child: Text(
                                  _taskProvider!.chosenLabelString,
                                  style: Theme.of(context)
                                      .textTheme
                                      .subtitle1!
                                      .copyWith(
                                          color: AppConstant.labelsColors[
                                              _taskProvider!
                                                  .chosenLabelString]),
                                ),
                              ),
                              SizedBox(
                                width: 5.sp,
                              ),
                              const Icon(
                                Icons.arrow_forward_ios,
                                size: 12.5,
                                color: AppConstant.mainColor,
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 45.sp,
                  ),
                  Row(
                    children: [
                      const Text("${AppConstant.timeSpent}: "),
                      SizedBox(
                        width: 5.sp,
                      ),
                      Text("${_taskProvider!.timeSpent}"),
                      SizedBox(
                        width: 10.sp,
                      ),
                      SizedBox(
                        width: 25.sp,
                        height: 25.sp,
                        child: IconButton(
                          onPressed: () {
                            if (!_taskProvider!.timerStatus) {
                              _taskProvider!.startTimer();
                            } else {
                              _taskProvider!.stopTimer();
                            }
                          },
                          padding: EdgeInsets.zero,
                          icon: Icon(
                            !_taskProvider!.timerStatus
                                ? Icons.play_arrow
                                : Icons.stop,
                            color: !_taskProvider!.timerStatus
                                ? AppConstant.green
                                : AppConstant.red,
                          ),
                        ),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 45.sp,
                  ),
                  Center(
                    child: MainButton(
                      onPressed: () async {
                        showLoading();
                        await _taskProvider!.editTask(
                            widget.task,
                            titleCont.text,
                            descriptionCont.text,
                            dropdownvalue);
                        Navigator.pop(context);
                        Navigator.pop(context);
                      },
                      text: AppConstant.editTask,
                    ),
                  )
                ],
              ),
            ),
          ),
        );
      },
    );
  }

  chooseLabel() {
    showModalBottomSheet(
        context: context,
        builder: (_) {
          return Column(
            children: [
              SizedBox(
                height: 5.sp,
              ),
              Center(
                child: Text(
                  AppConstant.chooseLabelTitle,
                  style: Theme.of(context).textTheme.titleLarge,
                ),
              ),
              SizedBox(
                height: 15.sp,
              ),
              InkWell(
                onTap: () {
                  _taskProvider!.setChosenLabel(0, context);
                  Navigator.pop(context);
                },
                child: Container(
                  decoration: BoxDecoration(
                    color: AppConstant.grey,
                    borderRadius: BorderRadius.circular(12.5),
                  ),
                  padding: EdgeInsets.all(10.sp),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Row(
                        children: [
                          const Icon(
                            Icons.label_important_rounded,
                            color: AppConstant.mainColor,
                          ),
                          SizedBox(
                            width: 5.sp,
                          ),
                          Container(
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(50),
                              color: Colors.red.withOpacity(0.2),
                            ),
                            padding: EdgeInsets.symmetric(
                                horizontal: 5.sp, vertical: 2.sp),
                            child: Text(
                              "Bug",
                              style: Theme.of(context)
                                  .textTheme
                                  .subtitle1!
                                  .copyWith(
                                    color: Colors.red,
                                  ),
                            ),
                          ),
                        ],
                      ),
                      Row(
                        children: const [
                          Icon(
                            Icons.arrow_forward_ios,
                            size: 12.5,
                            color: AppConstant.mainColor,
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
              SizedBox(
                height: 5.sp,
              ),
              InkWell(
                onTap: () {
                  _taskProvider!.setChosenLabel(1, context);
                  Navigator.pop(context);
                },
                child: Container(
                  decoration: BoxDecoration(
                    color: AppConstant.grey,
                    borderRadius: BorderRadius.circular(12.5),
                  ),
                  padding: EdgeInsets.all(10.sp),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Row(
                        children: [
                          const Icon(
                            Icons.label_important_rounded,
                            color: AppConstant.mainColor,
                          ),
                          SizedBox(
                            width: 5.sp,
                          ),
                          Container(
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(50),
                              color: Colors.green.withOpacity(0.2),
                            ),
                            padding: EdgeInsets.symmetric(
                                horizontal: 5.sp, vertical: 2.sp),
                            child: Text(
                              "Development",
                              style: Theme.of(context)
                                  .textTheme
                                  .subtitle1!
                                  .copyWith(
                                    color: Colors.green,
                                  ),
                            ),
                          ),
                        ],
                      ),
                      Row(
                        children: const [
                          Icon(
                            Icons.arrow_forward_ios,
                            size: 12.5,
                            color: AppConstant.mainColor,
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
              SizedBox(
                height: 5.sp,
              ),
              InkWell(
                onTap: () {
                  _taskProvider!.setChosenLabel(2, context);
                  Navigator.pop(context);
                },
                child: Container(
                  decoration: BoxDecoration(
                    color: AppConstant.grey,
                    borderRadius: BorderRadius.circular(12.5),
                  ),
                  padding: EdgeInsets.all(10.sp),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Row(
                        children: [
                          const Icon(
                            Icons.label_important_rounded,
                            color: AppConstant.mainColor,
                          ),
                          SizedBox(
                            width: 5.sp,
                          ),
                          Container(
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(50),
                              color: Colors.yellow.withOpacity(0.2),
                            ),
                            padding: EdgeInsets.symmetric(
                                horizontal: 5.sp, vertical: 2.sp),
                            child: Text(
                              "Design",
                              style: Theme.of(context)
                                  .textTheme
                                  .subtitle1!
                                  .copyWith(
                                    color: Colors.yellow,
                                  ),
                            ),
                          ),
                        ],
                      ),
                      Row(
                        children: const [
                          Icon(
                            Icons.arrow_forward_ios,
                            size: 12.5,
                            color: AppConstant.mainColor,
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
            ],
          );
        });
  }

  showLoading() {
    showDialog(
        context: context,
        builder: (_) {
          return const Loading();
        });
  }
}
