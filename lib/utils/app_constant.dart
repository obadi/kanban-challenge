import 'package:flutter/material.dart';

class AppConstant {
  ///Colors
  static const Color mainColor = Colors.black;
  static const Color grey = Colors.black12;
  static const Color white = Colors.white;
  static const Color green = Colors.green;
  static const Color red = Colors.red;
  static const Color offWhite = Color.fromRGBO(242, 245, 255, 1);

  ///Texts
  static const String homePageTitle = 'Main Board';
  static const String welcome = "Welcome";
  static const String todo = "To Do";
  static const String inProgress = "In Progress";
  static const String done = "Done";
  static const String label = "Label";
  static const String titleHint = "Task Title";
  static const String descriptionHint = "description...";
  static const String addTaskButton = "Add Task";
  static const String authTitle = "Welcome to Obada's Kanban Board";
  static const String logInButton = "Sign in with Google";
  static const String chooseLabelTitle = "Choose a Label";
  static const String emptyTaskTitleError = "Please enter a task title";
  static const String addCard = "Add Card";
  static const String editTask = "Save Changes";
  static const String timeSpent = "Time spent";



  static const Map<String, Color> labelsColors = {
    'Bug': Colors.red,
    'Development': Colors.green,
    'Design': Colors.yellow,
  };
}