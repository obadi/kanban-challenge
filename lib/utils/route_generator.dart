import 'package:flutter/material.dart';
import 'package:kanban_board_challenge/screens/add_task_screen/presentation/add_task_screen.dart';
import 'package:kanban_board_challenge/screens/auth_screen/presentation/auth_screen.dart';
import 'package:kanban_board_challenge/screens/home_screen/presentation/home_screen.dart';
import 'package:kanban_board_challenge/screens/splash_screen/presentation/splash_screen.dart';
import 'package:kanban_board_challenge/screens/task_screen/presentation/task_screen.dart';

class RouteGenerator {
  static Route<dynamic> generateRoute(RouteSettings settings) {
    final args = settings.arguments;

    switch (settings.name) {
      case RouteNames.splashScreen:
        return MaterialPageRoute(
          builder: (_) => const SplashScreen(),
        );
      case RouteNames.homeScreen:
        return MaterialPageRoute(
          builder: (_) => const MyHomePage(),
        );
      case RouteNames.authScreen:
        return MaterialPageRoute(
          builder: (_) => const AuthScreen(),
        );
      case RouteNames.taskScreen:
        if (args is Map) {
          return MaterialPageRoute(
            builder: (_) => TaskScreen(
              task: args['task'],
            ),
          );
        }
        return _errorRoute();
      case RouteNames.addTaskScreen:
        if (args is Map) {
          return MaterialPageRoute(
            builder: (_) => AddTaskScreen(
              boardName: args['boardName'],
            ),
          );
        }
        return _errorRoute();
      default:
        return _errorRoute();
    }
  }

  static Route<dynamic> _errorRoute() {
    return MaterialPageRoute(builder: (_) {
      return Scaffold(
        appBar: AppBar(
          title: const Text('Error'),
        ),
        body: const Center(
          child: Text('ERROR'),
        ),
      );
    });
  }
}

class RouteNames {
  static const splashScreen = '/';
  static const homeScreen = '/home_screen';
  static const authScreen = '/auth_screen';
  static const taskScreen = '/home_screen/task_screen';
  static const addTaskScreen = '/home_screen/add_task_screen';
}
