import 'package:flutter/material.dart';
import 'package:kanban_board_challenge/utils/app_constant.dart';
import 'package:sizer/sizer.dart';

class Loading extends StatelessWidget {
  const Loading({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Container(
            height: 50.sp,
            width: 50.sp,
            decoration: BoxDecoration(
                color: AppConstant.offWhite,
                borderRadius: BorderRadius.circular(8.0)),
            child: SizedBox(
              height: 50.sp,
              width: 50.sp,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: const [
                  CircularProgressIndicator(
                    color: AppConstant.mainColor,
                  )
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
