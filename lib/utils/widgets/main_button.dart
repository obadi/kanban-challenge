import 'package:flutter/material.dart';
import 'package:kanban_board_challenge/utils/app_constant.dart';
import 'package:sizer/sizer.dart';

class MainButton extends StatelessWidget {
  final Function? onPressed;
  final String? text;

  const MainButton({Key? key, this.onPressed, this.text}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialButton(
      onPressed: (){
        onPressed!();
        // _taskProvider!.signInLogic(context);
      },
      minWidth: 150.sp,
      height: 40.sp,
      color: AppConstant.mainColor,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(50.0),
      ),
      child: Text(
        text!,
        style: Theme.of(context).textTheme.button!.copyWith(
          color: AppConstant.white,
        ),
      ),
    );
  }
}
